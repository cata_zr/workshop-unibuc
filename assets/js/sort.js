function sort(element) {
	var currentUrl = window.location.href;
	var parsedUrl = $.url(currentUrl);
	var params = parsedUrl.param();
	params["sort"] = $(element).val();

	var newUrl = "?" + $.param(params);
	document.location.href = newUrl;
}

$(document).ready(function() {
	$('.rating span').hover(
		function(){
			$(this).prevAll(':not(.selected)').removeClass('icon-star-empty').addClass('icon-star');
			$(this).not('.selected').removeClass('icon-star-empty').addClass('icon-star');
		}, 
		function(){
			$(this).prevAll(':not(.selected)').removeClass('icon-star').addClass('icon-star-empty');
			$(this).not('.selected').removeClass('icon-star').addClass('icon-star-empty');
		}
	);

	$('.rating span').click(function(){
		var $this = $(this);
		$.post('/~unibuc1/index.php/ajax/save_rating', {movieid: $(this).parent().data('movieid'), rating: $(this).data('rating')}, function(data) {
			var data = jQuery.parseJSON(data);
			if (!data.error) {
				$this.siblings().removeClass('selected').removeClass('icon-star').addClass('icon-star-empty');

				$this.prevAll().addClass('selected').removeClass('icon-star-empty').addClass('icon-star');
				$this.addClass('selected').removeClass('icon-star-empty').addClass('icon-star');
			}
		});
	});
});