<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* solr config vars */
$config['solr'] = array(
    'host'  => '127.0.0.1',
    'port'  => 8080,
    'path'  => '/solr/collection1/'
);

$config['solr_ratings'] = array(
    'host'  => '127.0.0.1',
    'port'  => 8080,
    'path'  => '/solr/collection2/'
);

$config['solr_ratings_by_movie'] = array(
    'host'  => '127.0.0.1',
    'port'  => 8080,
    'path'  => '/solr/collection3/'
);