<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (($solr_cmd = $this->input->post('solr_cmd')) !== false) {
            if ($solr_cmd == 'do_index') {        
                $this->config->load('user_conf');
                $this->load->model('admin_model');
                $this->load->library('Solr/Solr_Client', $this->config->item('solr'));

                $movies = $this->admin_model->getMovies();

                foreach ($movies as $field) {
                    $doc = new Apache_Solr_Document();

                    $keywords = $this->db->query("SELECT k.* FROM movies_keywords mk INNER JOIN keywords k ON mk.keyword_id = k.id WHERE movieid = ".$field['movieid'])->result_array();
                    foreach ($keywords as $keyword) {
                        $doc->addField("keywords_en", $keyword['name']);
                    }

                    $genres = $this->db->query("SELECT g.* FROM movies_genres mg INNER JOIN genres g ON mg.genre_id = g.id WHERE movieid = ".$field['movieid'])->result_array();
                    foreach ($genres as $genre) {
                        $doc->addField("genres_en", $genre['name']);
                    }

                    $actors = $this->db->query("SELECT a.* FROM movies_actors ma INNER JOIN actors a ON ma.actor_id = a.id WHERE movieid = ".$field['movieid']." ORDER BY ma.id ASC LIMIT 10")->result_array();
                    foreach ($actors as $actor) {
                        $doc->addField("actors_en", $actor['name']);
                    }

                    foreach ($field as $name => $value) {
                        $ft = '_en';

                        if (is_numeric($value)) {
                            $ft = (strpos($value, '.') === false) ? '_i' : '_f';
                        } else {
                            $value = strtolower($value);
                        }

                        if ($name == 'movieid') {
                            $name = 'id';
                        } else {
                            $name .= $ft;
                        }

                        $doc->addField($name, $value);
                    }

                    $this->solr_client->addDocument($doc);
                }

                $this->solr_client->commit();

                echo '<script type="text/javascript">alert("Indexing done!");</script>';
            }
        }

        $this->load->view('partials/header');
        $this->load->view('admin_content');
        $this->load->view('partials/footer');
    }

	public function index_ratings() {
		$this->config->load('user_conf');
		$this->load->model('admin_model');
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		$this->load->library('Solr/Solr_Client', $this->config->item('solr_ratings'));

		$this->solr_client->deleteByQuery('*:*'); 
		$this->solr_client->commit();

		$movies = $this->admin_model->getRatings();

		foreach ($movies as $field) {
			$doc = new Apache_Solr_Document();

			foreach ($field as $name => $value) {
				if ($name=="count") continue;
				$ft = '_en';

				if ($name == 'userid') {
					$name = 'id';
				} elseif ($name=='movies') {
					$name .= $ft;
				}

				if ($name=="movies_en") {
					 $values = explode(",", $value);
					 foreach ($values as $value) {
						 $doc->addField($name, $value);
					 }
					 continue;
				}

				$doc->addField($name, $value);
			}

			$this->solr_client->addDocument($doc);
		}

		$this->solr_client->commit();

		echo '<script type="text/javascript">alert("Indexing done!");</script>';
	}

	public function index_ratings_new() {
		$this->config->load('user_conf');
		$this->load->model('admin_model');
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		$this->load->library('Solr/Solr_Client', $this->config->item('solr_ratings_by_movie'));

		$this->solr_client->deleteByQuery('*:*'); 
		$this->solr_client->commit();

		$movies = $this->admin_model->getRatingsByMovie();

		foreach ($movies as $field) {
			$doc = new Apache_Solr_Document();

			foreach ($field as $name => $value) {
				if ($name=="count") continue;
				$ft = '_en';

				if ($name == 'movieid') {
					$name = 'id';
				} elseif ($name=='users') {
					$name .= $ft;
				}

				if ($name=="users_en") {
					 $values = explode(",", $value);
					 foreach ($values as $value) {
						 $doc->addField($name, $value);
					 }
					 continue;
				}

				$doc->addField($name, $value);
			}

			$this->solr_client->addDocument($doc);
		}

		$this->solr_client->commit();

		echo '<script type="text/javascript">alert("Indexing done!");</script>';
	}

    public function getKeywords() {
        $this->load->model('movies_model');
        ini_set('max_execution_time', 0);
        $movies = $this->movies_model->getTopMovies(4000,10000);

        require_once('TMDB/Client.php');
        require_once('TMDB/structures/Asset.php');
        require_once('TMDB/structures/Collection.php');
        require_once('TMDB/structures/Company.php');
        require_once('TMDB/structures/Genre.php');
        require_once('TMDB/structures/Movie.php');
        require_once('TMDB/structures/Person.php');

        $db = TMDB\Client::getInstance('e157c15ae0265bb223b92e645563bd6a');

        $db->adult = false;  // return adult content
        $db->paged = true; // merges all paged results into a single result automatically

        foreach ($movies as $movie) {
            $title = $movie['title'];
            $year = $movie['year'];

            $results = $db->search('movie', array('query'=>$title, 'year'=>$year));

            $movie_obj = reset($results);

            if (is_object($movie_obj)) $keywords = $movie_obj->keywords();
            if (is_object($keywords)) {
                foreach ($keywords->keywords as $keyword) {
                    $this->db->replace('keywords', array(
                            'id'    => $keyword->id,
                            'name'  => $keyword->name
                        ));

                    $this->db->replace('movies_keywords', array(
                            'movieid'    => $movie['movieid'],
                            'keyword_id'  => $keyword->id
                        ));
                }
            }

            echo $movie['title'].'<br>';
            ob_flush();
        }
    }

    public function getGenres() {
        $this->load->model('movies_model');
        ini_set('max_execution_time', 0);

        require_once('TMDB/Client.php');
        require_once('TMDB/structures/Asset.php');
        require_once('TMDB/structures/Collection.php');
        require_once('TMDB/structures/Company.php');
        require_once('TMDB/structures/Genre.php');
        require_once('TMDB/structures/Movie.php');
        require_once('TMDB/structures/Person.php');

        $db = TMDB\Client::getInstance('e157c15ae0265bb223b92e645563bd6a');

        $genres = new TMDB\structures\Genre('movie');
        
        if (is_object($genres)) {
            foreach ($genres->genres as $genre) {
                $movies = new TMDB\structures\Genre($genre->id);
                for ($i=1; $i<=100; $i++) {
                    foreach ($movies->movies(null, $i) as $movie) {
                        $movie_row = $this->db->query("SELECT * FROM movies WHERE title = ".$this->db->escape($movie->title)."")->row_array();
                        if (!$movie_row) {
                            continue;
                        }
                        $this->db->replace('movies_genres', array(
                                'movieid'    => $movie_row['movieid'],
                                'genre_id'  => $genre->id
                            ));
                    }
                }
            }
        }
    }

    public function getCasts() {
        $this->load->model('movies_model');
        ini_set('max_execution_time', 0);
        $movies = $this->movies_model->getTopMovies(0,4000);

        require_once('TMDB/Client.php');
        require_once('TMDB/structures/Asset.php');
        require_once('TMDB/structures/Collection.php');
        require_once('TMDB/structures/Company.php');
        require_once('TMDB/structures/Genre.php');
        require_once('TMDB/structures/Movie.php');
        require_once('TMDB/structures/Person.php');

        $db = TMDB\Client::getInstance('e157c15ae0265bb223b92e645563bd6a');

        $db->adult = false;  // return adult content
        $db->paged = true; // merges all paged results into a single result automatically

        foreach ($movies as $movie) {
            $title = $movie['title'];
            $year = $movie['year'];

            $results = $db->search('movie', array('query'=>$title, 'year'=>$year));

            $movie_obj = reset($results);

            if (is_object($movie_obj)) $casts = $movie_obj->casts();
            if (is_array($casts)) {
                foreach ($casts['cast'] as $actor) {
                    $this->db->replace('actors', array(
                            'id'    => $actor->id,
                            'name'  => $actor->name
                        ));

                    $this->db->replace('movies_actors', array(
                            'movieid'    => $movie['movieid'],
                            'actor_id'  => $actor->id
                        ));
                }
            }
        }
    }
}