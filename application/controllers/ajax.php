<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller 
{
	public function save_rating() {
		$movieid = $_POST['movieid']; 
		$rating = $_POST['rating'];

		if (isset($rating) && is_numeric($rating) && $rating <= 10 && $rating >= 1 && $movieid) {
			$ok = $this->db->replace("ratings", array(
				'userid'	=> ip2long($_SERVER['REMOTE_ADDR']),
				'movieid'	=> $movieid,
				'rating'	=> $rating,
				'time'		=> date('Y-m-d H:i:s'),
			));
			if ($ok) {
				echo json_encode(array("error" => 0, 'message' => "Successfully saved rating."));
				die();
			}
			else {
				echo json_encode(array("error" => 1, 'message' => "Could not save rating."));
				die();
			}
		}
		else {
			echo json_encode(array("error" => 1, 'message' => "Incorrect rating."));
			die();
		}
	}
}