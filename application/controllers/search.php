<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($offset=0)
    {
        if (!empty($_GET['q'])) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $this->config->load('user_conf');
            $this->config->load('pagination');
            $this->load->library('pagination');
            $this->load->model('movies_model');
            $this->load->helper('sort');

            $this->load->library('Solr/Solr_Client', $this->config->item('solr'));

            $params = array(
                'fl'        => 'id, title_en, year_i, avg_rating_f',
                'wt'        => 'json',
                'qf'        => 'title_en^100 genres_en^20 year_i^10 actors_en^50 keywords_en^10',
                'defType'   => 'edismax',
                'rows'      => $this->config->item('per_page'),
                'bf'        => 'product(count_rating_i)',
            );

            $sort = getSort();
            if ($sort['criteria']=="popular") {
                $params['sort'] = 'score DESC, count_rating_i DESC';
            }
            if ($sort['criteria']=="rating") {
                $params['sort'] = 'avg_rating_f '.$sort['direction'];
            }
            if ($sort['criteria']=="date") {
                $params['sort'] = 'year_i '.$sort['direction'];
            }

            $response   = $this->solr_client->search('title_en: ' . strtolower($_GET['q']). '' , $offset, $this->config->item('per_page'), $params);
            $json       = $response->getRawResponse();
            $raw        = json_decode($json, true);

            foreach ($response->response->docs as $movie) {
                $movie_ids[] = $movie->id;
            }
            if (!empty($movie_ids))
                $movies = $this->movies_model->getMoviesById($movie_ids);
            else 
                $movies = array();
        } else {
            die('Well hello there. How did you got here?');
        }

        $all_movies = $this->movies_model->countMovies();

        $config['base_url'] = site_url('search');
        $config['total_rows'] = $response->response->numFound;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $this->load->view('partials/header');
        $this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => $sort));
        $this->load->view('partials/footer');
    }
}