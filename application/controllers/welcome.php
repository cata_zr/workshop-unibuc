<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function index($offset=0)
	{
		$this->load->model('movies_model');
		$this->load->library('pagination');
		$this->load->helper('sort');

		$sort = getSort();

		if ($sort['criteria']=="popular") {
			$movies = $this->movies_model->getTopMovies($offset,40);
		}

		if ($sort['criteria']=="date") {
			$movies = $this->movies_model->getMovies($offset,40,'year',$sort['direction']);
		}

		if ($sort['criteria']=="rating") {
			$movies = $this->movies_model->getMovies($offset,40,'avg_rating',$sort['direction']);
		}

		$all_movies = $this->movies_model->countMovies($sort['criteria']);

		$config['base_url'] = site_url('movies');
		$config['total_rows'] = $all_movies;

		$this->pagination->initialize($config);

		$pagination = $this->pagination->create_links();

		$this->load->view('partials/header');
		$this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => $sort));
		$this->load->view('partials/footer');
	}

	public function movie($id) {
		$this->config->load('user_conf');
        $this->config->load('pagination');
        $this->load->library('pagination');
        $this->load->model('movies_model');
        $this->load->helper('sort');

        $this->load->library('Solr/Solr_Client', $this->config->item('solr'));

        $params = array(
            'fl'        => 'id, title_en, year_i, count_rating_i, avg_rating_f, mlt_genres,mlt_keywords,mlt_actors,title_en',
            'wt'        => 'json',
            'rows'      => 7,
            'mlt'		=> 'true',
            'mlt.fl'	=> 'mlt_genres,mlt_keywords,mlt_actors, title_en',
            'mlt.mintf'	=> 1,
            'mlt.mindf' => 2,
            'mlt.minwl'	=> 5,
            'mlt.count'	=> 7,
            'mlt.boost'	=> 'true',
            'mlt.qf'	=> 'mlt.genres^500 mlt_keywords^100 mlt.actors^50 titl_en',
            'mlt.interestingTerms' => 'list',
        );

        $sort = getSort();

        $response   = $this->solr_client->search('id: ' . $id, 0, 1, $params);
        $json       = $response->getRawResponse();
        $raw        = json_decode($json, true);

        $movie_ids[9999999] = $id;
        foreach ($response->moreLikeThis->$id->docs as $movie) {
            $movie_ids[$movie->count_rating_i*$movie->avg_rating_f] = $movie->id;
        }

        // krsort($movie_ids);

        if (!empty($movie_ids))
            $movies = $this->movies_model->getMoviesById($movie_ids);
        else 
            $movies = array();

        $all_movies = $this->movies_model->countMovies();

        $config['base_url'] = site_url('search');
        $config['total_rows'] = $response->response->numFound;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $this->load->view('partials/header', array('movies' => $movies, 'moreLikeThis' => true));
        $this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => $sort, 'moreLikeThis' => true));
        $this->load->view('partials/footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */