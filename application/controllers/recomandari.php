<?php

class Recomandari extends CI_Controller {
	public function text_based($offset=0) {
		$this->load->model('movies_model');
		$this->config->load('user_conf');
		$this->config->load('pagination');
		$this->load->library('pagination');
		$this->load->model('movies_model');
		$this->load->helper('sort');
		$this->load->library('Solr/Solr_Client', $this->config->item('solr'));
		$ratings = $this->movies_model->getUserRatings();
		$all_ratings = $this->movies_model->getAllUserRatings();

		foreach ($ratings as $rating) {
			$movie_ids[] = $rating['movieid'];
		}

		foreach ($all_ratings as $rating) {
			$all_movie_ids[] = $rating['movieid'];
		}

		if (isset($movie_ids) && count($movie_ids)) {
			$fq = implode(" ", $movie_ids);

			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fq'				=> 'id : ('.$fq.')',
				'facet'				=> 'true',
				'facet.field'		=> array('mlt_actors','mlt_genres','mlt_keywords')
			);

			$response   = $this->solr_client->search('*:*', 0, 0, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);
		
			$facets = $raw['facet_counts']['facet_fields'];
			foreach ($facets as $field => $array) {
				$i=0;
				foreach ($array as $value => $boost) {
					$results[$field][$value] = $boost;
					if ($i>=6) break;
					$i++;
				}
			}

			$bq = ' ';
			foreach ($results['mlt_actors'] as $value => $boost) {
				$bq .= 'actors_en:"'.$value.'"^'.($boost*2500).' ';
			}

			foreach ($results['mlt_genres'] as $value => $boost) {
				$bq .= 'mlt_genres:"'.$value.'"^'.($boost*1500).' ';
			}

			foreach ($results['mlt_keywords'] as $value => $boost) {
				$bq .= 'mlt_keywords:"'.$value.'"^'.($boost*1000).' ';
			}

			$fq = implode(" ", $all_movie_ids);

			$params2 = array(
				'wt'        => 'json',
				'defType'   => 'edismax',
				'fq'		=> '-id:('.$fq.') AND avg_rating_f: [7 TO *]',
				'bq'		=> $bq,
				//'boost'     => 'count_rating_i',
			);

			$response2   = $this->solr_client->search('*:*', $offset, $this->config->item('per_page'), $params2);
			$json       = $response2->getRawResponse();
			$raw        = json_decode($json);

			$movie_ids = array();
			foreach ($response2->response->docs as $movie) {
				$movie_ids[] = $movie->id;
			}
		}
	
		if (!empty($movie_ids))
			$movies = $this->movies_model->getMoviesById($movie_ids);
		else 
			$movies = array();

		$all_movies = $this->movies_model->countMovies();

        $config['base_url'] = site_url('recomandari/text_based');
        $config['total_rows'] = isset($raw->response->numFound)?$raw->response->numFound:0;
		$config['uri_segment'] = 3;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $this->load->view('partials/header');
        $this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => array()));
        $this->load->view('partials/footer');

	}

	public function collaborative_filtering($offset=0) {
		$this->load->model('movies_model');
		$this->config->load('user_conf');
		$this->config->load('pagination');
		$this->load->library('pagination');
		$this->load->model('movies_model');
		$this->load->helper('sort');
		$this->load->library('Solr/Solr_Client', $this->config->item('solr_ratings'));

		$ratings = $this->movies_model->getUserRatings();
		$all_ratings = $this->movies_model->getAllUserRatings();

		foreach ($ratings as $rating) {
			$movie_ids[] = $rating['movieid'];
		}

		foreach ($all_ratings as $rating) {
			$all_movie_ids[] = $rating['movieid'];
		}

		if (isset($movie_ids) && count($movie_ids)) {

			$fq = implode(" ", $movie_ids);

			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fq'				=> '-id: '.ip2long($_SERVER['REMOTE_ADDR']),
				'fl'				=> 'id, score'
			);

			$response   = $this->solr_client->search('movies_en : ('.$fq.')', 0, 100, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);
	
			echo '<pre>';
			print_r($raw);
			die();

			$i = 0;

			$bq = ' ';
			foreach ($response->response->docs as $user) {
				if ($i==0) $max_score = $user->score;
				$users[] = $user->id;
				$bq .= 'id:'.$user->id.'^'.round($user->score,2).' ';
				if ($user->score*3<$max_score) break;
				$i++;
			}
		
			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fq'				=> 'id: ('.implode(" ", $users).')',
				'fl'				=> 'id, score',
				'bq'				=> $bq,
				'facet'				=> 'true',
				'facet.field'		=> 'movies_en',
				'facet.mincount'	=> '1',
			);

			$response   = $this->solr_client->search('*:*', 0, 0, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);

			$movies = array();
			foreach ($raw['facet_counts']['facet_fields']['movies_en'] as $movie => $boost) {
				$movies[$movie] = $boost;
			}

			$bq = ' ';
			foreach ($movies as $value => $boost) {
				$bq .= 'id:'.$value.'^'.($boost*10).' ';
			}

			$fq = implode(" ", $all_movie_ids);

			$this->load->library('Solr/Solr_Client', $this->config->item('solr'));
			$this->solr_client = new Solr_Client($this->config->item('solr'));

			$params2 = array(
				'wt'        => 'json',
				'defType'   => 'edismax',
				'fq'		=> '-id:('.$fq.') AND avg_rating_f: [5 TO *] AND count_rating_i: [25 TO *]',
				'bq'		=> $bq,
				'fl'		=> 'id,title_en',
			);

			$response2   = $this->solr_client->search('*:*', $offset, $this->config->item('per_page'), $params2);
			$json       = $response2->getRawResponse();
			$raw        = json_decode($json);

			$movie_ids = array();
			foreach ($response2->response->docs as $movie) {
				$movie_ids[] = $movie->id;
			}
		}
	
		if (!empty($movie_ids))
			$movies = $this->movies_model->getMoviesById($movie_ids);
		else 
			$movies = array();

		$all_movies = $this->movies_model->countMovies();

        $config['base_url'] = site_url('recomandari/collaborative_filtering');
        $config['total_rows'] = isset($raw->response->numFound)?$raw->response->numFound:0;
		$config['uri_segment'] = 3;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $this->load->view('partials/header');
        $this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => array()));
        $this->load->view('partials/footer');

	}

	public function collaborative_filtering_new($offset=0) {
		$this->load->model('movies_model');
		$this->config->load('user_conf');
		$this->config->load('pagination');
		$this->load->library('pagination');
		$this->load->model('movies_model');
		$this->load->helper('sort');
		$this->load->library('Solr/Solr_Client', $this->config->item('solr_ratings_by_movie'));

		$ratings = $this->movies_model->getUserRatings();
		$all_ratings = $this->movies_model->getAllUserRatings();

		foreach ($ratings as $rating) {
			$movie_ids[] = $rating['movieid'];
		}

		foreach ($all_ratings as $rating) {
			$all_movie_ids[] = $rating['movieid'];
		}

		if (isset($movie_ids) && count($movie_ids)) {
			$fq = implode(" ", $movie_ids);

			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fq'				=> 'id: ('.$fq.') AND -users_en: ('.ip2long($_SERVER['REMOTE_ADDR']).')',
				'fl'				=> 'id, score',
				'facet'				=> 'true',
				'facet.field'		=> 'users_en',
				'facet.mincount'	=> '1',
			);			

			$response   = $this->solr_client->search('*:*', 0, 100, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);
			echo '<pre>';
			print_r($raw);
			die();

			$i = 0;

			$bq = ' ';
			$users = array();
			$i = 0;
			foreach ($raw['facet_counts']['facet_fields']['users_en'] as $user => $boost) {
				$users[$user] = $boost*1000;
				if ($i==0) $max_boost = $boost;
				if ($boost*2<$max_boost) break;
				$i++; 
			}
			foreach ($users as $user => $boost) {
				$bq .= 'id:'.$user.'^'.$boost.' ';
			}

			$this->solr_client = new Solr_Client($this->config->item('solr_ratings'));
		
			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fl'				=> 'id, score, movies_en',
				'bq'				=> $bq,
				'facet'				=> 'true',
				'facet.field'		=> 'movies_en',
				'facet.mincount'	=> '1'
			);

			$response   = $this->solr_client->search('*:*', 0, 0, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);

			$movie_ids = array();
			foreach ($raw['facet_counts']['facet_fields']['movies_en'] as $movie => $boost) {
				$movie_ids[] = $movie;
			}
		}
	
		if (!empty($movie_ids))
			$movies = $this->movies_model->getMoviesById($movie_ids);
		else 
			$movies = array();

		$all_movies = $this->movies_model->countMovies();

        $config['base_url'] = site_url('recomandari/collaborative_filtering');
        $config['total_rows'] = isset($raw->response->numFound)?$raw->response->numFound:0;
		$config['uri_segment'] = 3;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $this->load->view('partials/header');
        $this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => array()));
        $this->load->view('partials/footer');

	}

	public function hybrid($offset=0) {
		$this->load->model('movies_model');
		$this->config->load('user_conf');
		$this->config->load('pagination');
		$this->load->library('pagination');
		$this->load->model('movies_model');
		$this->load->helper('sort');
		$this->load->library('Solr/Solr_Client', $this->config->item('solr_ratings'));
		$ratings = $this->movies_model->getUserRatings();
		$all_ratings = $this->movies_model->getAllUserRatings();

		foreach ($ratings as $rating) {
			$movie_ids[] = $rating['movieid'];
		}

		foreach ($all_ratings as $rating) {
			$all_movie_ids[] = $rating['movieid'];
		}

		if (isset($movie_ids) && count($movie_ids)) {
			$fq = implode(" ", $movie_ids);

			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fq'				=> '-id: '.ip2long($_SERVER['REMOTE_ADDR']),
				'fl'				=> 'id, score, movies_en'
			);

			$response   = $this->solr_client->search('movies_en : ('.$fq.')', 0, 100, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);

			$i = 0;

			$bq = ' ';
			foreach ($response->response->docs as $user) {
				if ($i==0) $max_score = $user->score;
				$users[] = $user->id;
				$bq .= 'id:'.$user->id.'^'.round($user->score,2).' ';
				if ($user->score*3<$max_score) break;
				$i++;
			}
		
			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fq'				=> 'id: ('.implode(" ", $users).')',
				'fl'				=> 'id, score',
				'bq'				=> $bq,
				'facet'				=> 'true',
				'facet.field'		=> 'movies_en',
				'facet.mincount'	=> '1',
			);

			$response   = $this->solr_client->search('*:*', 0, 0, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);

			$movies = array();
			foreach ($raw['facet_counts']['facet_fields']['movies_en'] as $movie => $boost) {
				$movies[$movie] = $boost;
			}

			$bq = ' ';
			foreach ($movies as $value => $boost) {
				$bq .= 'id:'.$value.'^'.($boost*50).' ';
			}

			$fq = implode(" ", $all_movie_ids);

			$this->load->library('Solr/Solr_Client', $this->config->item('solr'));
			$this->solr_client = new Solr_Client($this->config->item('solr'));

			$ratings = $this->movies_model->getUserRatings();
			$all_ratings = $this->movies_model->getAllUserRatings();

			foreach ($ratings as $rating) {
				$movie_ids[] = $rating['movieid'];
			}

			foreach ($all_ratings as $rating) {
				$all_movie_ids[] = $rating['movieid'];
			}

			$fq = implode(" ", $movie_ids);

			$params = array(
				'wt'				=> 'json',
				'defType'			=> 'edismax',
				'fq'				=> 'id : ('.$fq.')',
				'facet'				=> 'true',
				'facet.field'		=> array('mlt_actors','mlt_genres','mlt_keywords')
			);

			$response   = $this->solr_client->search('*:*', 0, 0, $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json,true);
		
			$facets = $raw['facet_counts']['facet_fields'];
			foreach ($facets as $field => $array) {
				$i=0;
				foreach ($array as $value => $boost) {
					$results[$field][$value] = $boost;
					if ($i>=10) break;
					$i++;
				}
			}

			foreach ($results['mlt_actors'] as $value => $boost) {
				$bq .= 'actors_en:"'.$value.'"^'.($boost*150).' ';
			}

			foreach ($results['mlt_genres'] as $value => $boost) {
				$bq .= 'mlt_genres:"'.$value.'"^'.($boost*100).' ';
			}

			foreach ($results['mlt_keywords'] as $value => $boost) {
				$bq .= 'mlt_keywords:"'.$value.'"^'.($boost*150).' ';
			}

			$params2 = array(
				'wt'        => 'json',
				'defType'   => 'edismax',
				'fq'		=> '-id:('.$fq.') AND avg_rating_f: [5 TO *] AND count_rating_i: [50 TO *]',
				'bq'		=> $bq,
				'fl'		=> 'id,title_en',
			);

			$response2   = $this->solr_client->search('*:*', $offset, $this->config->item('per_page'), $params2);
			$json       = $response2->getRawResponse();
			$raw        = json_decode($json);

			$movie_ids = array();
			foreach ($response2->response->docs as $movie) {
				$movie_ids[] = $movie->id;
			}
		}
	
		if (!empty($movie_ids))
			$movies = $this->movies_model->getMoviesById($movie_ids);
		else 
			$movies = array();

		$all_movies = $this->movies_model->countMovies();

        $config['base_url'] = site_url('recomandari/hybrid');
        $config['total_rows'] = isset($raw->response->numFound)?$raw->response->numFound:0;
		$config['uri_segment'] = 3;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $this->load->view('partials/header');
        $this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => array()));
        $this->load->view('partials/footer');

	}

	public function my_ratings($offset=0) {
		$this->load->model('movies_model');
		$ratings = $this->movies_model->getUserRatings();
		$all_ratings = $this->movies_model->getAllUserRatings();

		foreach ($all_ratings as $rating) {
			$movie_ids[] = $rating['movieid'];
		}

		$this->config->load('user_conf');
		$this->config->load('pagination');
		$this->load->library('pagination');
		$this->load->model('movies_model');
		$this->load->helper('sort');

		$this->load->library('Solr/Solr_Client', $this->config->item('solr'));

		if (isset($movie_ids) && count($movie_ids)) {

			$params = array(
				'fl'        => 'id, title_en, year_i, avg_rating_f',
				'wt'        => 'json',
				'defType'   => 'edismax',
				'fq'		=> 'id: ('.implode(" ", $movie_ids).')',
				'rows'      => $this->config->item('per_page'),
			);

			$sort = getSort();
			if ($sort['criteria']=="popular") {
				$params['sort'] = 'score DESC, count_rating_i DESC';
			}
			if ($sort['criteria']=="rating") {
				$params['sort'] = 'avg_rating_f '.$sort['direction'];
			}
			if ($sort['criteria']=="date") {
				$params['sort'] = 'year_i '.$sort['direction'];
			}

			$response   = $this->solr_client->search('*:*' , $offset, $this->config->item('per_page'), $params);
			$json       = $response->getRawResponse();
			$raw        = json_decode($json, true);

			$movie_ids = array();
			foreach ($response->response->docs as $movie) {
				$movie_ids[] = $movie->id;
			}
		}

		if (!empty($movie_ids))
			$movies = $this->movies_model->getMoviesById($movie_ids);
		else 
			$movies = array();

        $all_movies = $this->movies_model->countMovies();

        $config['base_url'] = site_url('search');
        $config['total_rows'] = isset($response->response->numFound)?$response->response->numFound:0;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();
		if (!isset($sort)) $sort = array();

        $this->load->view('partials/header');
        $this->load->view('pages/homepage', array('movies' => $movies, 'pagination' => $pagination, 'count' => $all_movies, 'sort' => $sort));
        $this->load->view('partials/footer');
	}

}