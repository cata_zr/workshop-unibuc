<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($movie_id)
    {
        $this->load->database();
        $this->load->model('movies_model');
        $this->load->helper('url');
        $this->load->helper('file');

        $movie = $this->movies_model->getMovieById($movie_id);
        if ($movie['poster']) {
			if (file_exists(FCPATH.'posters'.$movie['poster'])) {
				header('Content-type: image/jpg');
				echo file_get_contents(FCPATH.'posters'.$movie['poster']);
				die();
			}
        }

        require_once('TMDB/Client.php');
        require_once('TMDB/structures/Asset.php');
        require_once('TMDB/structures/Collection.php');
        require_once('TMDB/structures/Company.php');
        require_once('TMDB/structures/Genre.php');
        require_once('TMDB/structures/Movie.php');
        require_once('TMDB/structures/Person.php');

        $db = TMDB\Client::getInstance('e157c15ae0265bb223b92e645563bd6a');

        $db->adult = true;  // return adult content
        $db->paged = true; // merges all paged results into a single result automatically

        $title = $movie['title'];
        $year = $movie['year'];

        $results = $db->search('movie', array('query'=>$title, 'year'=>$year));

        $movie = reset($results);

        if (is_object($movie)) $images = $movie->posters(300);

        if (isset($images[0])) {

            $url = parse_url($images[0]->file_path);
            $filename = FCPATH.'posters'.$url['path'];
            $directory_path = dirname($filename);
            if (!file_exists($directory_path))
                mkdir($directory_path, 0777, TRUE);

            if (!file_exists($filename)) {
                $image = file_get_contents($images[0]->file_path);
                file_put_contents($filename, $image);
            }
            else {
                $image = file_get_contents($filename);
            }
            
            $this->db->where('movieid', $movie_id);
            $this->db->update('movies', array('poster' => $url['path']));
            
            header('Content-type: image/jpg');
            echo $image;
			die();
        }
        else {
            header('Content-type: image/jpg');
            $image = file_get_contents('assets/img/noposter.png');
            echo $image;
        }
        die();
    }
}