<?php

function getSort() {
	if (isset($_GET['sort'])) {
		if (strpos($_GET['sort'], 'desc')!==FALSE) $sort['direction'] = 'desc';
		if (strpos($_GET['sort'], 'asc')!==FALSE) $sort['direction'] = 'asc';
		$sort['criteria'] = str_replace(array('asc', 'desc'), '', $_GET['sort']);
	}

	if (!isset($sort['criteria'])) $sort['criteria'] = 'popular';

	return $sort;
}