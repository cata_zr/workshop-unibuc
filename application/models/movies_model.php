<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies_Model extends CI_Model
{
    private $table;

    public function __construct()
    {
        parent::__construct();

        $this->_table = array(
            'movies' => 'movies'
        );
    }

    public function getMovieById($movie_id) {
        $movie = $this->db->query("SELECT * FROM movies WHERE movieid = ".$this->db->escape($movie_id))->row_array();
        return $movie;
    }

    public function getTopMovies($offset=0, $limit=40, $min_rating_count=10) {
        $this->load->database();

        $result = $this->db->query('SELECT m.*, CAST(avg_rating AS UNSIGNED) AS R, count_rating AS v, r.rating
                                    FROM movies m
                                    LEFT JOIN ratings r ON m.movieid = r.movieid AND r.userid = '.ip2long($_SERVER['REMOTE_ADDR']).'
                                    GROUP BY m.movieid
                                    ORDER BY ((v / (v+25000)) * R + (25000 / (v+25000)) * 7) DESC, V DESC, R DESC
                                    LIMIT '.(int)$offset.', '.$this->db->escape($limit));
        foreach ($result->result_array() as $row) {
            $movies[] = $row;
        }

        return $movies;
    }

    public function countMovies($criteria='popular') {
        $this->load->database();

        if ($criteria=='popular') {
            $row = $this->db->query('SELECT COUNT(*) as count
                                    FROM movies m WHERE count_rating > 25')->row_array();
        }
        else {
            $row = $this->db->query('SELECT COUNT(*) as count
                                    FROM movies m WHERE count_rating > 25')->row_array();
        }

        return $row['count'];
    }

    public function getMoviesById($ids,$offset=0, $limit=40) {
        $this->load->database();

        $result = $this->db->query("SELECT m.*, r.rating FROM movies m LEFT JOIN ratings r ON m.movieid = r.movieid AND r.userid = ".ip2long($_SERVER['REMOTE_ADDR'])." WHERE m.movieid IN (".implode(",", $ids).") ORDER BY FIELD (m.movieid, ".implode(",", $ids).") LIMIT ".(int)$offset.", ".$this->db->escape($limit));
        foreach ($result->result_array() as $row) {
            $movies[] = $row;
        }

        return $movies;
    }   

    public function getMovies($offset=0,$limit=40,$sort_field='year',$sort_direction='desc')
    {
        $this->load->database();

        $result = $this->db->query('SELECT m.*, FLOOR(avg_rating) AS R, count_rating AS v, r.rating
                                    FROM movies m
                                    LEFT JOIN ratings r ON m.movieid = r.movieid AND r.userid = '.ip2long($_SERVER['REMOTE_ADDR']).'
									WHERE count_rating > 25
                                    GROUP BY m.movieid
                                    ORDER BY '. ($sort_field=='year'?'year ':'R ') .$sort_direction.', ((v / (v+25000)) * R + (25000 / (v+25000)) * 7) DESC, V DESC
                                    LIMIT '.(int)$offset.', '.$this->db->escape($limit));
        foreach ($result->result_array() as $row) {
            $movies[] = $row;
        }
        
        return $movies;
    }

	public function getUserRatings() {
		$results = $this->db->query("SELECT * FROM ratings WHERE userid = ".ip2long($_SERVER['REMOTE_ADDR'])." AND rating > 7 ORDER BY time DESC");
		
		return $results->result_array();
	}

	public function getAllUserRatings() {
		$results = $this->db->query("SELECT * FROM ratings WHERE userid = ".ip2long($_SERVER['REMOTE_ADDR'])." ORDER BY time DESC");
		
		return $results->result_array();
	}
}