<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Model extends CI_Model
{
    private $table;

    public function __construct()
    {
        parent::__construct();

        $this->_table = array(
            'movies' => 'movies',
			'ratings' => 'ratings'
        );
    }

    public function getMovies()
    {
        $this->load->database();

        $query = $this->db->get($this->_table['movies']);

        $results = array();
        foreach ($query->result() as $row)
        {
            $results[] = (array)$row;
        }
        
        return $results;
    }

	public function getRatings()
    {
        $this->load->database();

        $query = $this->db->query("SELECT userid, GROUP_CONCAT(movieid) as movies, COUNT(movieid) as count FROM ratings WHERE rating >= 6 GROUP BY userid HAVING count >= 5");

        $results = array();
        foreach ($query->result() as $row)
        {
            $results[] = (array)$row;
        }
        
        return $results;
    }

	public function getRatingsByMovie()
    {
        $this->load->database();

        $query = $this->db->query("SELECT movieid, GROUP_CONCAT(userid) as users, COUNT(movieid) as count FROM ratings WHERE rating >= 6 GROUP BY movieid");

        $results = array();
        foreach ($query->result() as $row)
        {
            $results[] = (array)$row;
        }
        
        return $results;
    }
}