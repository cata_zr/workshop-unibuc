<div class="bs-docs-example" style="width: 266px; padding-top: 45px;">
    <div class="label-e">Indexing movies</div>
    <font style="font-size: 18px;">Push this button</font>
    <i class="fa fa-caret-right" style="margin: 0px 5px; font-size: 18px;"></i>
    <form method="POST" style="display: inline-block; margin-bottom: 10px;">
        <input type="text" name="solr_cmd" value="do_index" style="display: none; visibility: hidden;" />
        <input type="submit" class="btn btn-danger" style="margin-top: -4px; font-size: 16px;" value="Don't do it" />
    </form>
</div>