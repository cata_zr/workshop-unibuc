<li class="span3">
    <div class="thumbnail" style="height: 400px; <?php if (($moreLikeThis) && !$i) : ?>background: #ccc;<?php endif; ?>">
        <a href="<?php echo site_url('movie/'.url_title($movie['title'].'-'.$movie['movieid'])); ?>">
            <img alt="300x200" src="<?php echo site_url('image/get/'.$movie['movieid']); ?>">

            <div class="caption">
                <h6><?php echo character_limiter($movie['title'], 30); ?> (<?php echo $movie['year']; ?>)</h6>

                <p>Rating: <?php echo $movie['avg_rating']; ?></p>
            </div>
        </a>
        <div class="rating" data-movieid="<?php echo $movie['movieid']; ?>">
            <?php for ($i=1; $i<=10; $i++) : ?>
                <span data-rating="<?php echo $i; ?>" class="icon-star<?php if ($i>$movie['rating']) : ?>-empty<?php endif; ?>"></span>
            <?php endfor; ?>
        </div>
    </div>
</li>