<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" context="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title><?php if (isset($_GET['q'])) : ?>Search: <?php echo htmlspecialchars($_GET['q']); ?> - <?php endif; ?>
    <?php if (isset($moreLikeThis)) : ?>More like: <?php echo htmlspecialchars($movies[0]['title']); ?> - <?php endif; ?>
    Movie recommender workshop</title>
    <meta name="description" content="">

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-theme.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/app.css') ?>" rel="stylesheet">
</head>
<body>

    <div id="header">
        <div class="navbar navbar-static-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>


                    <div class="nav-collapse collapse">
                        <form action="javascript:void(0);" id="search-form" data-url="<?php echo site_url('search') ?>" class="navbar-form pull-left">
                            <div class="input-append">
                                <input class="span3" id="search-fraze" type="text" value="<?php if (isset($_GET['q'])) echo htmlspecialchars($_GET['q']); ?>" />
                                <button id="do-search" class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                        <ul class="nav">
                            <li class="active"><a href="<?php echo site_url('movies') ?>">Movies</a></li>
							<li class="active"><a href="<?php echo site_url('recomandari/text_based') ?>">TB Reco</a></li>
							<li class="active"><a href="<?php echo site_url('recomandari/collaborative_filtering') ?>">CF Reco (good)</a></li>
							<li class="active"><a href="<?php echo site_url('recomandari/collaborative_filtering_new') ?>">CF Reco (bad)</a></li>
							<li class="active"><a href="<?php echo site_url('recomandari/hybrid') ?>">Hybrid Reco</a>
							<li class="active"><a href="<?php echo site_url('recomandari/my_ratings') ?>">My ratings</a>
							<li class="active"><a href="<?php echo site_url('admin') ?>">Admin</a>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
