</div> <!-- container end -->

<footer class="footer">
<div class="container">
<div class="row-fluid">
  <div class="span6"> <p class="copyright"><img class="foot-logo" src="/assets/img/logo-ts.png" alt="Brand Logo"/> &copy; 2013 <strong>Watchopolis</strong> Video Portal Theme - Crafted by <a href="https://wrapbootstrap.com/user/themestrap">Themestrap</a> </p></div>
<div class="span6">   <div class="pagination pull-right">
</div>
</div>
</div>
</div>
</footer>



    <!-- Include Javascript/JQuery Assets
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
 <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/jquery.jkit.1.1.10.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/jquery.easing.1.3.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/purl.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/sort.js'); ?>"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#do-search').on('click', function(event) {
            event.preventDefault();
            doSearch();
        });

        $('#search-fraze').on('keyup', function(event) {
            if (event.keyCode == 13) {
                doSearch();
            }
        });


        $("[data-rel=tooltip]").tooltip();
        // $('body').jKit();
    });

    function doSearch()
    {
        var search_fraze = $('#search-fraze').val().trim();
        $('#search-fraze').removeAttr('style');

        if (search_fraze != '') {
            var url = $('#search-form').data('url') + '/?q=' + search_fraze;
            window.location.href = url;
        } else {
            $('#search-fraze').css('box-shadow', '0px 0px 4px 0.5px red');
        }
    }
</script>

  </body>
</html>
