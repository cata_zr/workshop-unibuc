    <!-- Start Info Unit -->

    <!-- <div id="info-unit">
        <div class="alert alert-block alert-info fade in">
            <button class="close" data-dismiss="alert" type=
            "button">×</button>

            <h4 class="alert-heading">Your one stop destination for all
            things you watch.</h4>

            <p>Change this and that and try again. Duis mollis, est non
            commodo luctus, nisi erat porttitor ligula, eget lacinia odio
            sem nec elit. Cras mattis consectetur purus sit amet
            fermentum.</p>

            <p><a class="btn btn-primary" href="#">Join Watchopolis</a>
            <a class="btn btn-success" href="#">Or do this</a></p>
        </div>
    </div> -->
    <!-- End Info Unit -->

    <div>
        <div class="row-fluid">
            <div class="span9 main-section">
                <!-- Options bar (Left Section) -->

                <div class="pagination pull-left" style="margin-top: 0px;">
                    <?php echo $pagination; ?>
                </div>
                <div style="clear: both"></div>

                <div class="options-bar">
                    <select class="span2 pull-left" onchange="sort(this)">
                        <option value="">
                            Sort By
                        </option>

                        <option value="datedesc" <?php if (isset($_GET['sort']) && $_GET['sort']=="datedesc") echo "selected"; ?>>
                            Date
                        </option>

                        <option value="populardesc" <?php if ((isset($_GET['sort']) && $_GET['sort']=="populardesc") || !isset($_GET['sort'])) echo "selected"; ?>>
                            Popular
                        </option>

                        <option value="ratingdesc" <?php if (isset($_GET['sort']) && $_GET['sort']=="ratingdesc") echo "selected"; ?>>
                            Highest Rating
                        </option>

                        <option value="ratingasc" <?php if (isset($_GET['sort']) && $_GET['sort']=="ratingasc") echo "selected"; ?>>
                            Lowest Rating
                        </option>
                    </select><span><strong><?php echo $count; ?></strong> Movies</span>
                </div><!-- End Options Bar -->
                <!-- Start Left Section -->

                <div class="row-fluid">
                    <ul class="thumbnails">
                        <?php $i = 0; ?>
                        <?php foreach ($movies as $movie) : ?>
                            <?php echo $this->load->view('partials/movie', array('movie' => $movie, 'i' => $i, 'moreLikeThis' => isset($moreLikeThis)), true); ?>
                            <?php $i++; ?>
                        <?php endforeach; ?>                        
                    </ul>
                </div>

                <div class="pagination pagination-small pull-right">
                    <?php echo $pagination; ?>
                </div>
            </div><!-- End Left Section -->
            <!-- Start Right Section -->

            <div class="span3" id="side-section">
                <ul class="nav nav-list">
                    <li class="nav-header">filter by</li>

                    <li class="active">
                        <a href="<?php echo site_url('movies') ?>?sort=datedesc"><i class="icon-arrow-down"></i> Recently added</a>
                    </li>

                    <li>
                        <a href="<?php echo site_url('movies') ?>"><i class="icon-heart"></i> Most Popular</a>
                    </li>

                    <li>
                        <a href="<?php echo site_url('movies') ?>?sort=ratingdesc"><i class="icon-star"></i> Highest Rating</a>
                    </li>
                </ul>

                
            </div>
        </div>
        <hr>
    </div>
</div>